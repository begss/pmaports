# Maintainer: Raffaele Tranquillini <raffaele.tranquillini@gmail.com>

pkgname=device-pegatron-chagall
pkgdesc="Pegatron Chagall"
pkgver=3
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="armv7"
options="!check !archcheck"
depends="
	alsa-ucm-conf
	asus-transformer-blobtools
	libvdpau-tegra
	linux-postmarketos-grate
	mesa-dri-gallium
	mkbootimg
	postmarketos-base
"
makedepends="devicepkg-dev"
source="
	deviceinfo
	rootston.ini
"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-phosh
	$pkgname-x11
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel postmarketos-ui-phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

nonfree_firmware() {
	pkgdesc="Pegatron Chagall WiFi & Bluetooth firmware"
	depends="firmware-asus-transformer"
	mkdir "$subpkgdir"
}

x11() {
	install_if="$pkgname=$pkgver-r$pkgrel xorg-server"
	depends="xf86-video-opentegra"
	mkdir "$subpkgdir"
}

sha512sums="
d303d2a4ef5f55f09cf6f4af9449aaf00b3d94299cbf9f23c1a61c34478e64c98094f9a4416013c52f9519b5a775dead697babb3f031f12cc6a1a9e8f2d42474  deviceinfo
618284cdaccd09e60cb9a99afa337fcad7b3bd33f6422b9eae34175bc4516138d486cbb9f5735cafb325bc16cf362de16aeae7c77d334668c749afcfa557359b  rootston.ini
"
